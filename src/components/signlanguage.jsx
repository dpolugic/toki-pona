import React from "react";
import styled, { css } from "styled-components";

import signs from "../signs.json";


const Container = styled.div`
  max-width: 800px;
  margin: 0 auto 1rem;
  padding: 0 1rem;
`;

const TextArea = styled.textarea`
  display: block;
  width: 100%;
  height: 300px;
`;

const CardSectionContainer = styled.div`
  width: 100%;
  margin: 0 auto 1rem;
  padding: 0 1rem;

  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const CardContainer = styled.div`
  position: relative;
  width: 150px;
  height: 200px;
`;

const CardImage = styled.img`
  display: block;
  width: 100%;
`;

const CardTitle = styled.h4`
  margin: 0;
  font-weight: 700;
  text-shadow: 1px 1px 0 #fff;
  transition: text-shadow 0.2s ease;
`;

const CardInstructions = styled.span`
  opacity: 0;
  transition: opacity 0.2s ease;
`;

const CardTextContainer = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  padding: 0.5rem;
  transition: background 0.2s ease;

  ${({invalid}) =>
    invalid ? css`
      background: rgba(255, 0, 0, 0.8);
      &:hover {
        background: rgba(255, 0, 0, 0.5);
      }
    ` : css`
      background: transparent;
      &:hover {
        background: rgba(200, 200, 200, 0.8);
      }
    `}

  &:hover ${CardInstructions} {
    opacity: 1;
  }

  &:hover ${CardTitle} {
    text-shadow: none;
  }
`;

const WordCard = React.memo(({word = ""}) => (
  <CardContainer>
    {!!signs[word] ? (
      <>
      <CardImage src={`/sign-images/${word}.jpg`} />
      <CardTextContainer>
        <CardTitle>{word}</CardTitle>
        <CardInstructions>{signs[word]}</CardInstructions>
      </CardTextContainer>
      </>
    ) : (
      <CardTextContainer invalid>
        <CardTitle>{word}</CardTitle>
      </CardTextContainer>
    )}
  </CardContainer>
));


const splitAndFilterWords = text => text.split(/\s+/g).filter(Boolean);

// Split string into array of (word, count)-tuples, where `count` represents
// how many times this word has appeared before in the array.
// We can use this for more performant React keys.
// Example: "a b a a b" -> [["a", 0], ["b", 0], ["a", 1], ["a", 2], ["b", 1]]
const getWordsWithCount = text => {
  const counts = {};

  return splitAndFilterWords(text).map(word => {
    const count = (counts[word] || 0);
    counts[word] = count + 1;

    return [word, count];
  });
}

// Split string into array of (word, index)-tuples, where `index` is
// the index of this word in the array.
// Example: "a b a a b" -> [["a", 0], ["b", 1], ["a", 2], ["a", 3], ["b", 4]]
const getWordsWithIndex = text => {
  return splitAndFilterWords(text).map((word, i) => [word, i]);
}

const ALL_WORDS = Object.keys(signs).join(" ");

export const SignLanguage = () => {
  const [text, setText] = React.useState(ALL_WORDS);

  // Try adding or removing a word at the beginning of a very long sentence with both versions.
  const words = getWordsWithCount(text);
  // const words = getWordsWithIndex(text);

  return (
    <>
    <Container>
      <h1>Toki Pona Sign Language</h1>

      <label htmlFor="toki-pona-textarea">Toki Pona</label>
      <TextArea
        id="toki-pona-textarea"
        value={text}
        onChange={e => setText(e.target.value)}
      />
    </Container>
    <CardSectionContainer>
      {words.map(([word, c]) => (
        <WordCard key={`${word}-${c}`} word={word} />
      ))}
    </CardSectionContainer>
    <Container>
    </Container>
    </>
  );
};

