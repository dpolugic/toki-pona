import React, { useState, useEffect } from "react";
import { tpToEn } from "../tp-to-en";
import styled from "styled-components";
import { navBarHeight, font } from '../common'

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: ${navBarHeight};
  bottom: 0;
  left: 0;
  right: 0;

  &:last-child {
    margin-bottom: 1rem;
  }
`;

const Textarea = styled.textarea`
  margin: 1rem 1rem 0;
  padding: 0.5rem;
  flex: 1;
  resize: none;
  line-height: inherit;
  font-family: inherit;
  font-size: inherit;
  font-weight: inherit;
`;

const TextareaTp = styled(Textarea)`
  font-family: toki-pona;
  font-size: 1.25rem;

  &::placeholder {
    font-family: ${font};
  }
`;

export const Translate = () => {
  const [tp, setTp] = useState("");
  const [en, setEn] = useState("");

  useEffect(() => {
    tpToEn(tp).then(setEn);
  }, [tp]);

  return (
    <Wrap>
      <TextareaTp
        value={tp}
        onChange={(e) => setTp(e.target.value)}
        placeholder="toki pona"
        spellCheck={false}
        data-tabs-autoselect
      />
      <Textarea
        value={en}
        onChange={(e) => setEn(e.target.value)}
        placeholder="English"
      />
    </Wrap>
  );
};
