import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import React, { useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import { navBarHeight } from "../common";
import { items } from "../items";
import Tooltip from "@material-ui/core/Tooltip";

const sortAlphaNum = (a, b) => a.localeCompare(b, "en", { numeric: true });

const tags = Array.from(new Set(items.flatMap((item) => item.tags)))
  .filter((tag) => tag)
  .sort(sortAlphaNum);

const Wrap = styled.div`
  position: absolute;
  top: ${navBarHeight};
  bottom: 0;
  left: 0;
  right: 0;
  margin: 0 auto;
  padding: 0 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const Buttons = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const ButtonStyled = styled(Button)`
  && {
    margin: 0rem 0.75rem;
  }
`;

const TokiPona = styled.div`
  margin-top: 3rem;
  margin-bottom: 1rem;
  font-family: toki-pona;
  font-size: 2.5rem;
  height: 2.5rem;
`;

const PlainText = styled.div`
  font-size: 1.75rem;
`;

const Warn = styled(PlainText)`
  margin: 4rem 0;
`;

const Tags = styled.div`
  font-size: 0.875rem;
  color: #333;
  margin-top: 1rem;
  margin-bottom: 3rem;
`;

const getRandomItem = (tags) => {
  let itemsFiltered;
  if (!tags || tags.length === 0) {
    itemsFiltered = items;
  } else {
    itemsFiltered = items.filter((item) =>
      tags.some((tag) => item.tags && item.tags.includes(tag))
    );
  }

  return itemsFiltered[Math.floor(Math.random() * itemsFiltered.length)];
};

export const Practice = () => {
  const [item, setItem] = useState(getRandomItem());
  const [front, setFront] = useState("en");
  const [back, setBack] = useState("tp");
  const [selectedTags, setSelectedTags] = useState([]);

  const shuffle = useCallback(() => {
    setItem(getRandomItem(selectedTags));
    const heads = Math.random() < 0.5;
    setFront(heads ? "en" : "tp");
    setBack(heads ? "tp" : "en");
  }, [selectedTags]);

  useEffect(shuffle, [selectedTags]);

  const flip = useCallback(() => {
    setFront(back);
    setBack(front);
  }, [back, front]);

  useEffect(() => {
    const handler = (e) => {
      if (e.key === " " || e.key === "j") {
        flip();
      } else if (e.key === "Enter" || e.key === "k") {
        shuffle();
      }
    };

    window.addEventListener("keydown", handler);

    return () => window.removeEventListener("keydown", handler);
  }, [flip, shuffle]);

  return (
    <Wrap>
      <Autocomplete
        value={selectedTags}
        onChange={(event, newValue) => setSelectedTags(newValue)}
        style={{ width: "100%", maxWidth: "25rem" }}
        size="small"
        multiple
        options={tags}
        filterSelectedOptions
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            label="Tags to filter items"
            placeholder="Select tags"
          />
        )}
      />

      {item ? (
        <>
          <TokiPona>{front === "tp" && item[front]}</TokiPona>
          <PlainText>{item[front]}</PlainText>
          <Tags>{item.tags?.join(" ")}&nbsp;</Tags>
        </>
      ) : (
        <Warn>No items matching selected tags</Warn>
      )}
      <Buttons>
        <Tooltip title={`Keybind: Space / J`} arrow>
          <ButtonStyled variant="outlined" onClick={flip}>
            Flip
          </ButtonStyled>
        </Tooltip>

        <Tooltip title={`Keybind: Enter / K`} arrow>
          <ButtonStyled variant="outlined" onClick={shuffle}>
            Next
          </ButtonStyled>
        </Tooltip>
      </Buttons>
    </Wrap>
  );
};
