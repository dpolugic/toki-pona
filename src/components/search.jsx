import React, { useState, useEffect } from "react";
import styled from "styled-components";
import MiniSearch from "minisearch";
import { items } from "../items";

const Input = styled.input`
  width: 100%;
  text-align: center;
  outline: none;
  border: none;
  font-family: inherit;
  font-weight: inherit;
  font-size: 1.5rem;
  margin-top: 1rem;

  @media (max-width: 600px) {
    & {
      font-size: 1.5rem;
    }
  }
`;

const Result = styled.div`
  display: flex;
  margin: 1.25rem 0;
  line-height: 1;
`;

const ResultLeft = styled.div`
  text-align: right;
  flex: 1;
`;

const ResultRight = styled.div`
  flex: 1;
`;

const Tags = styled.span`
  font-size: 0.875rem;
  color: #333;
  margin-left: 0.5rem;
`;

const Gray = styled.span`
  color: #333;
`;

const ResultSep = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  user-select: none;
  color: #888;
  padding: 0 0.625rem;
`;

const TokiPona = styled.span`
  font-family: toki-pona;
  font-size: 1.5rem;
  margin-right: 0.5rem;
`

const documents = items.map((item, index) => ({ ...item, id: index }));

const Results = ({ searchString }) => {
  const [miniSearch, setMiniSearch] = useState()

  useEffect(() => {

    let newMiniSearch = new MiniSearch({
      fields: ["en", "tp", "tags"],
      storeFields: ["en", "tp", "tags"],
    });

    newMiniSearch.addAll(documents);

    setMiniSearch(newMiniSearch)
  }, [])

  const results = (searchString
    ? miniSearch.search(searchString, { prefix: true, fuzzy: 0.2 })
    : documents).slice(0, 20);

  return (
    <>
      {results.map(({ en, tp, id, tags }) => (
        <Result key={id}>
          <ResultLeft>{en}</ResultLeft>
          <ResultSep>–</ResultSep>
          <ResultRight>
            <TokiPona>{tp}</TokiPona>
            <Gray>{tp}</Gray>
            <Tags>{tags?.join(' ')}</Tags>
          </ResultRight>
        </Result>
      ))}
    </>
  );
};

export const Search = () => {
  const [searchString, setSearchString] = useState("");

  return (
    <>
      <Input
        placeholder="Search"
        value={searchString}
        onChange={(e) => setSearchString(e.target.value)}
        onKeyDown={(e) => /\d/.test(e.key) || e.stopPropagation()}
        data-tabs-autoselect
      />
      <Results searchString={searchString} />
    </>
  );
};
