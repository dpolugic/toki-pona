import React, { useState, createElement, useEffect } from "react";
import styled, { css } from "styled-components";
import { navBarHeight } from "../common";
import Tooltip from "@material-ui/core/Tooltip";

const TabsStyled = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  width: 100vw;
  height: ${navBarHeight};
  line-height: ${navBarHeight};
  text-align: center;
  background-color: #fff;
`;

const TabStyled = styled.span`
  margin: 0 1rem;

  @media (max-width: 600px) {
    & {
      margin: 0 0.5rem;
    }
  }

  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }

  ${(p) =>
    p.selected &&
    css`
      font-weight: 500;
    `}
`;

const Container = styled.div`
  padding-top: ${navBarHeight};

  ${(p) =>
    !p.selected &&
    css`
      display: none;
    `}
`;

export const Tabs = ({ tabs }) => {
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);

  useEffect(() => {
    const tab = document.getElementById(`tab-${selectedTabIndex}`);
    const element = tab && tab.querySelector("[data-tabs-autoselect]");
    element && element.focus();
  }, [selectedTabIndex]);

  useEffect(() => {
    const handler = (e) => {
      if (e.key === "1") {
        setSelectedTabIndex(0);
      } else if (e.key === "2") {
        setSelectedTabIndex(1);
      } else if (e.key === "3") {
        setSelectedTabIndex(2);
      } else if (e.key === "4") {
        setSelectedTabIndex(3);
      }
    };

    window.addEventListener("keydown", handler);

    return () => window.removeEventListener("keydown", handler);
  }, [setSelectedTabIndex]);

  return (
    <>
      <TabsStyled>
        {tabs.map((tab, index) => (
          <Tooltip title={`Keybind: ${index + 1}`} arrow key={index}>
            <TabStyled
              onClick={() => setSelectedTabIndex(index)}
              selected={index === selectedTabIndex}
            >
              {tab.title}
            </TabStyled>
          </Tooltip>
        ))}
      </TabsStyled>

      {tabs.map((tab, index) => (
        <Container
          id={`tab-${index}`}
          key={index}
          selected={index === selectedTabIndex}
        >
          {createElement(tabs[index].component)}
        </Container>
      ))}
    </>
  );
};
