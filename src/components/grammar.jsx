import React from "react";
import styled from "styled-components";

const Wrap = styled.div`
  max-width: 50rem;
  margin: 0 auto;
  padding: 0 1rem;

  ul {
    padding-left: 1.5rem;
  }

  li {
    padding-bottom: 0.5rem;
  }
`;

export const Grammar = () => (
  <Wrap>
    <h1>Toki Sona</h1>

    <p>
      Welcome to <strong>Toki Sona</strong> (“communicate wisely”): A set of
      tools for learning the <strong>Toki Pona</strong> language.
    </p>

    <h2>Grammar</h2>

    <ul>
      <li>
        Adjectives describe nouns. E.g., “jan pona” means “good person (friend)”
      </li>
      <li>
        “li” separates subject and verb. E.g., “jan li pona” means this “person
        is good”.
      </li>
      <li>
        All verbs are nouns. E.g., “moku” (“to eat”) becomes something that you
        eat, i.e. “food”.
      </li>
      <li>All nouns are adjectives. E.g., “tomo moku” is a room for eating.</li>
      <li>
        “e” separates the verb from the direct object. E.g., “moku e telo” means
        “drink water”, while “moku telo” means “liquid food (soup)”
      </li>
      <li>
        <strong>What questions:</strong> Use a regular sentence and place the
        word “seme” in the position where you want to know information. E.g.,
        “seme li sin” means “what is new?”.
      </li>
      <li>
        <strong>Yes/no questions 1)</strong> Add the phrase “anu seme” at the
        end.
      </li>
      <li>
        <strong>Yes/no questions 2)</strong> Repeat the word being used as a
        verb, adding “ala” in between. E.g., “ona li mama ala mama” is he/she a
        parent? Reply “yes” by repeating the verb. Reply “no” by repeating the
        verb with “ala” or use “ala” alone.
      </li>
      <li>
        The particle <strong>a</strong> adds emotion or emphasis. It can be used:
        <ul>
          <li>Alone</li>
          <li>At the end of a sentence.</li>
          <li>At the end of a noun phrase.</li>
        </ul>
      </li>
      <li>
        The particle <strong>o</strong> has three uses:
        <ol>
          <li>After a noun phrase to show who is being called or addressed.</li>
          <li>Before a verb to express a command or request.</li>
          <li>After the subject (and replacing “li”) to express a wish or desire.</li>
        </ol>
      </li>
      <li>
        <strong>Main verb and pre-verb:</strong>
        <p>Every sentence has a main verb. This is the verb or another word used in that position, such as an adjective, noun or preposition. The main verb is normally the word that comes after the particle li.</p>
        <p>A pre-verb can be added before the main verb:</p>
        <p>NOUN + li + PRE-VERB + VERB...</p>
        <p>E.g, “jan li wile pali...” meaning “Somebody wants to do...”</p>
        <p>Some pre-verbs have a slightly different meaning when they are used as an adjective or verb. For example “kama” and “lukin”.</p>
      </li>
      <li>
        <p>
          <strong>The particle “la”</strong> is very powerful. It allows you to link two sentences, or link a fragment to a sentence.
        </p>
        <p>
          A la B.<br />
          In the context of A, B.<br />
          If A, then B.
        </p>
        <p>
          The words after the preposition “lon” can be placed before “la” with the same meaning.
        </p>
        <p>See chapter 14 for examples.</p>
      </li>
    </ul>
  </Wrap>
);
