import React from "react";
import { Tabs } from "./tabs";
import { Translate } from "./translate";
import { Search } from "./search";
import { Practice } from "./practice";
import { Grammar } from "./grammar";
import { SignLanguage } from "./signlanguage";

export const Index = () => (
  <Tabs
    tabs={[
      { title: "Grammar", component: Grammar },
      { title: "Translate", component: Translate },
      { title: "Sign Language", component: SignLanguage },
      { title: "Search", component: Search },
      { title: "Practice", component: Practice },
    ]}
  />
);
