const unified = require("unified");
const latin = require("retext-latin");
const visit = require("unist-util-visit");
const stringify = require("retext-stringify");
const { items } = require("./items");

const simpleWord = () => (tree) =>
  visit(tree, "SentenceNode", (node) => {
    node.children = node.children.map((child) => {
      if (child.type !== "WordNode") {
        return child;
      }

      if (child.children.length !== 1) {
        return child;
      }

      if (child.children[0].type !== "TextNode") {
        return child;
      }

      return {
        ...child.children[0],
        type: "SimpleWordNode",
        translation: items.find(item => item.tp === child.children[0].value)?.en,
      };
    });
  });

const compoundSyntactic = (children, indexStart) => {
  let compounded = "";

  for (let i = indexStart; i < children.length; i++) {
    const child = children[i];
    const nextChild = children[i + 1];
    if (child.type === "SimpleWordNode") {
      compounded += child.value;
    } else if (
      compounded &&
      nextChild &&
      nextChild.type === "SimpleWordNode" &&
      child.type === "WhiteSpaceNode" &&
      child.value === " "
    ) {
      compounded += child.value;
    } else if (compounded) {
      return { compounded, offset: i };
    } else {
      return { compounded, offset: 0 };
    }
  }

  if (compounded) {
    return { compounded, offset: children.length };
  }

  return { compounded, offset: 0 };
};

const compoundSemantic = (compounded) => {
  const components = compounded.split(/\b/);

  let compoundStartsAtIndex;
  for (let i = components.length - 1; i >= 0; i--) {
    compoundStartsAtIndex = i
    if (items.find(item => item.tp === components.slice(0, i + 1).join(""))) {
      break;
    }
  }

  if (compoundStartsAtIndex > 0) {
    const value = components.slice(0, compoundStartsAtIndex + 1).join("");
    return {
      compoundNode: {
        type: "CompoundWordNode",
        value,
        translation: items.find(item => item.tp === value).en,
      },
      semanticOffset: compoundStartsAtIndex + 1,
    };
  }

  return { compoundNode: null, semanticOffset: 0 };
};

const compoundWord = () => (tree) =>
  visit(tree, "SentenceNode", (node) => {
    const newChildren = [];
    let i = 0;

    while (i < node.children.length) {
      const { compounded, offset } = compoundSyntactic(node.children, i);
      if (offset) {
        const { compoundNode, semanticOffset } = compoundSemantic(compounded);
        if (semanticOffset) {
          newChildren.push(compoundNode);
          i += semanticOffset;
        } else {
          newChildren.push(node.children[i]);
          i++;
        }
      } else {
        newChildren.push(node.children[i]);
        i++;
      }
    }

    node.children = newChildren;
  });

// const peek = () => (tree) => {
//   visit(tree, (node) => {
//     console.log(node.type, node.value);
//   });
// };

const translate = () => (tree) =>
  visit(tree, ["SimpleWordNode", "CompoundWordNode"], (node) => {
    const left = node.type === 'CompoundWordNode' ? '<' : '['
    const right = node.type === 'CompoundWordNode' ? '>' : ']'
    node.value = node.translation ? `${left}${node.translation}${right}` : `¿${node.value}?`;
  });

const processor = unified()
  .use(latin)
  .use(simpleWord)
  .use(compoundWord)
  // .use(peek)
  .use(translate)
  .use(stringify);

export const tpToEn = (tp) => processor.process(tp);
