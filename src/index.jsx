import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import React from "react";
import ReactDOM from "react-dom";
import { createGlobalStyle } from "styled-components";
import { font } from "./common";
import { Index } from "./components";
import "./index.css";
import * as serviceWorker from "./serviceWorker";

const theme = createMuiTheme({
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: "1rem",
      },
    },
  },
});

const GlobalStyle = createGlobalStyle`
  body {
    font-family: ${font};
    font-size: 1.125rem;
    line-height: 1.5;
    margin: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  @media (max-width: 600px) {
    body {
      font-size: 1rem;
    }
  }

  h1, h2, h3, strong {
    font-weight: 500;
  }
`;

ReactDOM.render(
  <React.StrictMode>
    <GlobalStyle />
    <MuiThemeProvider theme={theme}>
      <Index />
    </MuiThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
